<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pag1 extends CI_Controller {

    public function __construct() {
        parent:: __construct();
    }

    public function index() {
        $variaveis['titulo'] = "Pag 1";
        $variaveis['data'] = "23/07/2016";
        if ($this->input->is_ajax_request()) {
            $this->load->view('v_pag1', $variaveis);
        } else {
            $this->load->view('estrutura/e_cabecalho', $variaveis);
            $this->load->view('v_pag1', $variaveis);
            $this->load->view('estrutura/e_rodape', $variaveis);
        }
    }

}
