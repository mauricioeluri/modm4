<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?></div>
<div class="clear"></div>
<footer>
    <div id="itensRod">
        <div>
            <h6>E-mail</h6>
            <p>mauriciom.eluri@gmail.com</p>
        </div>
        <div class="linhaSubVer"></div>
        <div>
            <h6>Telefone</h6>
            <p> (53) 9936-8029</p>
        </div>
        <div class="linhaSubVer"></div>
        <div>
            <h6>Cidade</h6>
            <p>Bagé</p>
        </div>
        <div class="linhaSubVer"></div>
        <div>
            <h6>Social</h6>
            <span class="fa-stack fa-lg"><a href="https://bitbucket.org/mauricioeluri/" target="_blank">
                    <i class="fa fa-bitbucket-square fa-2x"></i></a>
            </span>
            <span class="fa-stack fa-lg"><a href="http://www.lnked.in/mauricioeluri" target="_blank">
                    <i class="fa fa-linkedin-square fa-2x"></i></a>
            </span>
            <span class="fa-stack fa-lg"><a href="http://www.facebook.com/mauricioeluri" target="_blank">
                    <i class="fa fa-facebook-square fa-2x"></i></a>
            </span>
            <p>@2016 por Maurício El Uri</p>
        </div>
    </div>
</footer>
</body>
</html>
<?= asset_css('main') ?>
<?= asset_js("bootstrap.min") ?>
<?= asset_js("main.min") ?>
<?= asset_css('fonts') ?>
<?= asset_css('font-awesome.min') ?>
