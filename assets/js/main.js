$(document).ready(function () {

    //ALTERAR CONTEÚDO    
    $('#content').load();
    //OUTROS ITENS DO MENU
    $('nav a').click(function (event) {
        event.preventDefault();
        //ALTERAR TITULO SITE
        var page = $(this).attr('href');
        titulo = page.concat(" - ELURI FRAMEWORK - mauricio.16mb.com");
        document.title = titulo;

        //COLOCA TITULO EM MINUSCULO PARA ABRIR O LINK
        var tit = page.toLowerCase();

        //TROCA O CONTEUDO DA PAGINA
        if (page === "#") {
            page = '';
            tit = '';
        } else {
            $('#content').load(tit);
            if (typeof (history.pushState) !== "undefined") {
                var obj = {Url: tit};
                history.pushState(obj, obj.Page, obj.Url);
            } else {
                alert("Por favor, atualize seu navegador para poder acessar este site corretamente.");
            }
            return false;
        }
    });

    // FECHAR MENU RESPONSIVO QUANDO CLICADO
    $(".link").click(function () {
        $(".navbar-collapse").collapse('hide');
    });

    /* ALTERAR LOGO HOVER */
    var sourceSwap = function () {
        var $this = $('.logo');
        var newSource = $this.data('alt-src');
        $this.data('alt-src', $this.attr('src'));
        $this.attr('src', newSource);
    };

    //ALTERAR LOGO    
    $('#log').hover(sourceSwap, sourceSwap);

    //DOWNLOAD
    $("#conteudo .btn-primary").click(function () {
        window.location.href = 'http://www.mauricio.16mb.com/MODM4_V3.3.zip';
    });

});
