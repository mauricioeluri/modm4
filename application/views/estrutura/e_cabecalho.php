<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
    <head>
        <title><?= $titulo ?> - MODM4 FRAMEWORK - mauricio.16mb.com</title>
        <link rel = "icon" type ="image/png" href = "<?= asset_img("logoSiteC.png") ?>" >
        <meta charset = "UTF-8">
        <meta name = description content = "O MODM4 Framework pode agilizar muito o seu desenvolvimento web. Contando com um simples modelo de website pronto, e com várias ferramentas e funções já configuradas.">
        <meta name = viewport content = "width=device-width, initial-scale=1">
        <?= asset_css('bootstrap.min') ?>
        <?= asset_css('estrutura') ?>
        <?= asset_js("jquery-2.1.4.min") ?>
        <!--[if lt IE 9]>
            <script>
                alert('Você está usando um navegador antigo!\n\nAlguns itens podem não aparecer corretamente.\nConsidere atualizar para uma versão mais recente.');
            </script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div id='topoTudo'>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="home">
                        <img class="navbar-brand logo" alt="Logo Site"
                             data-alt-src="<?= asset_img("logoSite2C.png") ?>"
                             src="<?= asset_img("logoSiteC.png") ?>"/>
                        <p class="navbar-brand">Maurício El Uri</p></a>
                </div>
                <div class="collapse navbar-collapse">
                    <div id='topoItem'>
                        <ul class="nav navbar-nav">
                            <li><a href="home" class="link">Home</a></li>
                            <li><a href="pag1" class="link">Pag1</a></li>
                            <li><a href="pag2" class="link">Pag2</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="pag1" class="link">Pag1</a></li>
                                    <li><a href="pag2" class="link">Pag2</a></li>
                                    <li><a href="home" class="link">Home</a></li>
                                    <li><a href="pag1" class="link">Pag1</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <h1>MODM4 V3.3</h1>
        <div id='content'>