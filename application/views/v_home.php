<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
    <section id="sliderhome">
        <div id="meuSlider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#meuSlider" data-slide-to="0"></li>
                <li data-target="#meuSlider" data-slide-to="1"></li>
                <li data-target="#meuSlider" data-slide-to="2"></li>
                <li data-target="#meuSlider" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class='item'><img src='<?= asset_img("rock1C.jpg") ?>' alt='Slider1'/></div>
                <div class='item'><img src='<?= asset_img("rock2C.jpg") ?>' alt='Slider2'/></div>
                <div class='item'><img src='<?= asset_img("rock3C.jpg") ?>' alt='Slider3'/></div>
                <div class='item active'><img src='<?= asset_img("rock4C.jpg") ?>' alt='Slider4'/></div>
            </div>
        </div>
    </section>
</div>
<div id="conteudo">
    <p>Bem-vindo ao meu framework!</p>
    <p>Atualizado dia: <?= $data ?></p>
    192.168.77.69<br>
    <br><button type="button" class="btn btn-lg btn-primary">Download</button><br><br>
    <a href="http://mauricio.16mb.com/antigos/">Download versões antigas</a><br/><br/>
    <p>VERSÃO 3.3 COM CODEIGNITER, AJAX E SASS!!!</p>
    <br>
    <a href="https://bitbucket.org/mauricioeluri/modm4" target="_blank" class="btn btn-info">
        <p style="color:#444;margin:0;">
            Colabore com o projeto :D
            <span class="fa-stack fa-lg">
                <i class="fa fa-bitbucket-square fa-2x"></i>
            </span>
        </p>
    </a>
</div>
<script>
    $(function () {
        $('.carousel').carousel({
            interval: 5000
        });
    });
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-81200143-1', 'auto');
    ga('send', 'pageview');
</script>
