<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent:: __construct();
    }

    public function index() {
        $variaveis['titulo'] = "Home";
        $variaveis['data'] = "23/07/2016";
        if ($this->input->is_ajax_request()) {
            $this->load->view('v_home', $variaveis);
        } else {
            $this->load->view('estrutura/e_cabecalho', $variaveis);
            $this->load->view('v_home', $variaveis);
            $this->load->view('estrutura/e_rodape', $variaveis);
        }
    }

}
