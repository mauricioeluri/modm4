<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function asset_url($param = null) {
    return base_url() . 'assets/' . $param;
}

function asset_js($param = null) {
    return '<script type="text/javascript" src="' . asset_url('js/' . $param) . '.js"></script>';
}

function asset_css($param = null) {
    return link_tag('assets/css/' . $param . '.css');
}

function asset_img($param = null) {
    return base_url('assets/img/' . $param);
}
